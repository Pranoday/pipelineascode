pipeline {
    agent any
    stages {
        stage('Pulling automation repository') 
        {
            steps 
            {
                
                    git credentialsId: 'MyGitlabCredentials', url: 'https://gitlab.com/Pranoday/pythonseleniumframework.git'
            
                    
            }
        }
        stage('Executing automation tests')
        {
            
            steps
            {
                
                    
                
                    //bat '''cd ./OrangeHRMDemoApp_Testcases
                     // pytest --html=../Reports/LoginTestcases.html -v'''
                    echo "${TestGroupName}"
                    bat """cd ./OrangeHRMDemoApp_Testcases
                    pytest -m ${TestGroupName} --html=../Reports/LoginTestcases.html -v"""
            }
        }
        /*stage('Sending report')
        {
            steps
            {
                
                    emailext body: 'Please find report of automation tests:Group:- ${TestGroupName}', subject: 'Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}', to: 'pranoday.dingare@gmail.com',attachmentsPattern:'./OrangeHRMDemoReport.html'
                
                
            }    
        }*/
            
    }

    
    post { 
        always { 
                           emailext body: 'Please find report of automation tests eecuted from Group: ${TestGroupName}', subject: "STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'", to: 'pranoday.dingare@gmail.com',attachmentsPattern:'**/OrangeHRMDemoReport.html'
                           //emailext body: 'Please find report of automation tests', subject: 'Python test result', to: 'pranoday.dingare@gmail.com'
        }
    }
    
}
